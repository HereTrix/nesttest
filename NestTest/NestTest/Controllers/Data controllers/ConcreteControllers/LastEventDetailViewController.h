//
//  LastEventDetailViewController.h
//  NestTest
//
//  Created by CityHall on 12/9/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraEvent.h"

@interface LastEventDetailViewController : UIViewController

@property (strong, nonatomic) CameraEvent *lastEvent;

@end
